package com.javagda21.spring_student_demo.controller;

import com.javagda21.spring_student_demo.model.Student;
import com.javagda21.spring_student_demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class StudentController {

    // jeśli klasa jest beanem czyli przez jaka kolwiek adnotacje - to autowires sprawdzi po nazwie klasy ktora odpowiada do nazwzy zmiennej
    // jesli sa dwa beany tej samej klasy to musi byc nazwa jesli jest tylko jedna to nazwa moze byc jaka kolwiek
    // beany sa szukane tylko w pakege glownym czyli com.javagda21.sping_student_demo
    @Autowired
    private StudentService studentService;

    @GetMapping("/student/list")
    public String getStudentList(Model model){
        model.addAttribute("studentList", studentService.read()); // przekazujemy (wysylamy do widoku html) liste studentow
        return "studentListPage";
    }

    @GetMapping("/student/add")
    public String getStudentAddPage() {
        return "studentAddPage";
    }

    @PostMapping("/student/add")
    public String postStudentAddPage(@RequestParam(name = "name") String name,
                                     @RequestParam(name = "surname") String surname) {

        Student student = new Student(null, name, surname, new ArrayList<>());
        studentService.add(student);
        return "redirect:/student/list";
    }

    @GetMapping("/student/remove")
    public String deleteStudent(@RequestParam(name = "id") int position) {
        studentService.remove(position);
        return "redirect:/student/list";
    }

    @GetMapping("/student/addGrades")
    public String getStudentAddGradesPage(@RequestParam(name = "id") int id,  Model model) {
        model.addAttribute("id", id);
        return "studentAddGradesPage";
    }

    @PostMapping("/student/addGrades")
    public String postStudentAddGradesPage(@RequestParam(name = "id") int id, @RequestParam(name = "grade") double grade) {
        final Student student = studentService.read().get(id);
        student.getGrades().add(grade);

        return "redirect:/student/list";
    }

    @GetMapping("/student/update")
    public String getStudentUpdatePage(@RequestParam(name = "id") int id, Model model) {
        model.addAttribute("id", id);
        return "studentUpdatePage";
    }

    @PostMapping("/student/update")
    public String postStudentUpdatePage(@RequestParam(name = "name") String name,
                                     @RequestParam(name = "surname") String surname,
                                        @RequestParam(name = "id") int id) {

        studentService.update(id, name, surname);
        return "redirect:/student/list";
    }
}
