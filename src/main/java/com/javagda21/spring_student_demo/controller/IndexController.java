package com.javagda21.spring_student_demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller // mówi o tym, że klasa jest klasą servletową - przesylanie i odbieranie danych do i z formularzy
public class IndexController {

    @GetMapping("/index") // adres na który ta metoda reaguje
                            // http://localhost:8080/index - na tą stronę będzie przekierowanie
    public String getIndexPage(){
        return "indexPage"; // zwraca adres HTML z pliku znajdującym się w templates
    }

    @GetMapping("/") // adres na który ta metoda reaguje
    public String getIndexPageRedirect(){
        return "redirect:/index"; // przekierowanie na metodę która posiada w GetMapping adres o tej nazwie
                                    // więc jak wpiszesz http://localhost:8080/ to i tak przekierowuje cię na @GetMapping("/index")
    }
}
