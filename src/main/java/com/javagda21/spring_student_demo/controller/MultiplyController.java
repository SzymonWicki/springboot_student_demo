package com.javagda21.spring_student_demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MultiplyController {

//    @RequestMapping(path = "/multiply", method = RequestMethod.GET) // @RequestMapping z określoną ścieżką path i method to to samo co @GetMapping
//    public String getMultiplyPageRequest(){
//        return "multiplyForm";
//    }

    @GetMapping("/multiply")
    public String getMultiplyPage(){
        return "multiplyForm"; // zwraca formularz multiplyForm.html
    }
    @PostMapping("/multiply")
    public String postMultiplyParameter(@RequestParam(name = "number1") int number1, // parametr metody name z formularza po czym deklarujemy zmienną // RequestParametr get parametr
                                        @RequestParam(name = "number2") int number2, Model model){ // model jest to wysylania atrybutow
                                                                                                    // to co wpiszemy w nawiasach, Spring będzie to wydobywał dla nas

        System.out.println("Liczba 1: " + number1); // wypisanie liczb wpisanych w formularzu do konsoli
        System.out.println("Liczba 2: " + number2);

        model.addAttribute("Liczba1", number1); // przekazanie aktybutu o nazwie Liczba1
        model.addAttribute("Liczba2", number2);

        return "multiplyForm";
    }
}
