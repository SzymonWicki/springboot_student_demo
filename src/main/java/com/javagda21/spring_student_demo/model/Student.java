package com.javagda21.spring_student_demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student { // w modelu trzymamy tylko dane które chcemy wysłac albo do theamleafa albo do bazy danych

    private Long id;
    private String name;
    private String surname;

    private List<Double> grades;
}
