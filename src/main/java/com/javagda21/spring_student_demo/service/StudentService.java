package com.javagda21.spring_student_demo.service;

import com.javagda21.spring_student_demo.model.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service // wykonuje cala logike
public class StudentService { // wszystkie komponenty sa singletonami - jedna instancja sie stworzyla i mozemy ja wykorzystywac
    // bean - fasolka - w javie to sa encje ktore sa przechowywane przez modul DI(dependency injection) i moga byc z niego wybierane w celu uzupelnienia zaleznosci
    // sa singletonami nazwanymi - posiadaja nazwy

    private List<Student> students = new ArrayList<>();

    // create

    public void add(Student student) {
        students.add(student);
    }


    public List<Student> read() {
        return students;
    }

    public void remove(int position) {
        students.remove(position);
    }

    public StudentService() {
        Student student1 = new Student(null, "a", "b", Arrays.asList(2.0, 3.0, 4.0, 5.0));
        Student student2 = new Student(null, "b", "c", Arrays.asList(2.0, 3.0, 4.0, 5.0));
        students.add(student1);
        students.add(student2);
    }


    public void update(int id, String name, String surname) {
        final Student student1 = students.get(id);
        student1.setName(name);
        student1.setSurname(surname);
    }
}
